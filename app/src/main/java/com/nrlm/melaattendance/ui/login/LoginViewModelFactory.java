package com.nrlm.melaattendance.ui.login;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;

public class LoginViewModelFactory implements ViewModelProvider.Factory {
    BaseRepository baseRepository;

    public LoginViewModelFactory(BaseRepository baseRepository)
    {
      this.baseRepository=baseRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        T loginViewModel = null;

        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            loginViewModel = (T) new LoginViewModel((AppRepository) baseRepository);
        }
        return loginViewModel;
    }
    }

