package com.nrlm.melaattendance.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;

import com.nrlm.melaattendance.databinding.FragmentAboutUsBinding;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.utils.BaseFragment;

public class AboutUsFragment extends BaseFragment<AboutUsViewModel, FragmentAboutUsBinding, AppRepository,AboutUsViewModelFactory> implements HomeActivity.OnBackPressedListener {
    LayoutInflater layouInflater;
    @Override
    public Class<AboutUsViewModel> getViewModel() {
        return AboutUsViewModel.class;
    }

    @Override
    public FragmentAboutUsBinding getFragmentBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        layouInflater=inflater;
        return FragmentAboutUsBinding.inflate(inflater,container,false);
    }

    @Override
    public AppRepository getFragmentRepository() {
        return AppRepository.getInstance(getActivity().getApplication());
    }

    @Override
    public Context getCurrentContext() {
        return getContext();
    }

    @Override
    public AboutUsViewModelFactory getFactory() {
        return new AboutUsViewModelFactory(getFragmentRepository());
    }

    @Override
    public void onFragmentReady() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void doBack() {
        NavDirections navDirections=AboutUsFragmentDirections.actionAboutUsFragmentToHomeFragment();
        navController.navigate(navDirections);

    }
}
