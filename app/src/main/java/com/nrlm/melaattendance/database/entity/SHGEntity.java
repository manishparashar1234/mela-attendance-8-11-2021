package com.nrlm.melaattendance.database.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
@Entity
public class SHGEntity {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public  int shgRegistrationId;
    public  String shgCode;
    public  String shgName;
    public  String loginMobilenumber;
    public int melaId;
    public String stallNo;
}
