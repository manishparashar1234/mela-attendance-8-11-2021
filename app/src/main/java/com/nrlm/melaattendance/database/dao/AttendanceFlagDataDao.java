package com.nrlm.melaattendance.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import com.nrlm.melaattendance.database.entity.AttendanceFlagDataEntity;

@Dao
public interface AttendanceFlagDataDao {
    @Insert
    void insertAll(AttendanceFlagDataEntity attendanceFlagDataEntity);

}
