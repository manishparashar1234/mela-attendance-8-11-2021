package com.nrlm.melaattendance.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.databinding.FragmentContactusBinding;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.utils.BaseFragment;

public class ContactUsFragment extends BaseFragment<ContactUsViewModel, FragmentContactusBinding, AppRepository,ContactUsViewModelFactory> implements HomeActivity.OnBackPressedListener{
    LayoutInflater layoutInflater;
    MenuItem menuItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((HomeActivity)getActivity()).setOnBackPressedListener(this);
    }

    @Override
    public Class<ContactUsViewModel> getViewModel() {
        return ContactUsViewModel.class;
    }

    @Override
    public FragmentContactusBinding getFragmentBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        layoutInflater=inflater;
        return FragmentContactusBinding.inflate(inflater,container,false);
    }

    @Override
    public AppRepository getFragmentRepository() {
        return AppRepository.getInstance(getActivity().getApplication());
    }

    @Override
    public Context getCurrentContext() {
        return getContext();
    }

    @Override
    public ContactUsViewModelFactory getFactory() {
        return new ContactUsViewModelFactory(getFragmentRepository());
    }

    @Override
    public void onFragmentReady() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }



    @Override
    public void doBack() {
        NavHostFragment navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.home_nav_host);
        navController = navHostFragment.getNavController();

NavDirections action =ContactUsFragmentDirections.actionContactUsFragmentToHomeFragment();

navController.navigate(action);


    }
}
