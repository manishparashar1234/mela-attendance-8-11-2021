package com.nrlm.melaattendance.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;
import com.nrlm.melaattendance.ui.login.LoginViewModel;

public class ContactUsViewModelFactory implements ViewModelProvider.Factory {
    BaseRepository baseRepository;

    public ContactUsViewModelFactory(BaseRepository baseRepository)
    {
        this.baseRepository=baseRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
    T  contactUsViewModel=null;
        if (modelClass.isAssignableFrom(ContactUsViewModel.class)) {
            contactUsViewModel = (T) new ContactUsViewModel((AppRepository) baseRepository);
        }
        return contactUsViewModel;
    }
}
