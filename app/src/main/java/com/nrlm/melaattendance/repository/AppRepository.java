package com.nrlm.melaattendance.repository;

import android.app.Application;

import androidx.annotation.NonNull;

import com.nrlm.melaattendance.database.AppDatabase;
import com.nrlm.melaattendance.database.dao.SHGDao;
import com.nrlm.melaattendance.database.entity.SHGEntity;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;



public class AppRepository extends BaseRepository{
    private static AppRepository mInstance;

    public static synchronized AppRepository getInstance(Application application) {
        if (mInstance == null) {
            mInstance = new AppRepository(application);
        }
        return mInstance;
    }
    private AppRepository(@NonNull Application application) {
        super(application);
        getAllInstance();

    }

    /****************Login data inserting in the DB*****************************/
    public void insertUserData(SHGEntity data) {
        AppDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                shgDao.insertAll(data);
            }
        });
    }

    /*******************************Fatching All shg Data form the Database***********************************/

    public List<SHGEntity> getAllShgData()
    {
List<SHGEntity> shgData=null;

try {
    Callable<List<SHGEntity>> callable = new Callable<List<SHGEntity>>() {
        @Override
        public List<SHGEntity> call() throws Exception {
            return shgDao.getShgAllData();
        }

    };
    Future<List<SHGEntity>> future = Executors.newSingleThreadExecutor().submit(callable);
    shgData=future.get();
}catch (Exception e)
{
    e.printStackTrace();
}
return shgData;
    }
}
