package com.nrlm.melaattendance.ui.mpin;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;

public class MpinViewModelFactory implements ViewModelProvider.Factory {
    BaseRepository baseRepository;

    public MpinViewModelFactory(BaseRepository baseRepository)
    {
        this.baseRepository=baseRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        T mpinViewModel=null;
        mpinViewModel=(T)new MpinViewModel((AppRepository)baseRepository);

        return mpinViewModel;
    }
}
