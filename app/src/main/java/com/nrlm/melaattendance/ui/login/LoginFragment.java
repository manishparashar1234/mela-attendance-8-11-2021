package com.nrlm.melaattendance.ui.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ViewUtils;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.JsonObject;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.database.entity.SHGEntity;
import com.nrlm.melaattendance.databinding.FragmentLoginBinding;
import com.nrlm.melaattendance.model.Request.LoginRequest;
import com.nrlm.melaattendance.network.volley.SingletonVolley;
import com.nrlm.melaattendance.network.volley.VolleyResult;
import com.nrlm.melaattendance.network.volley.VolleyService;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.ui.mpin.MpinActivity;
import com.nrlm.melaattendance.utils.AppConstants;
import com.nrlm.melaattendance.utils.AppDateFactory;
import com.nrlm.melaattendance.utils.AppDeviceInfoUtils;
import com.nrlm.melaattendance.utils.AppUtils;
import com.nrlm.melaattendance.utils.BaseFragment;
import com.nrlm.melaattendance.utils.Cryptography;
import com.nrlm.melaattendance.utils.DialogFactory;
import com.nrlm.melaattendance.utils.NetworkUtils;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ProjectPrefrences;
import com.nrlm.melaattendance.utils.ViewUtilsKt;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import static com.nrlm.melaattendance.network.volley.VolleyService.volleyService;

public class LoginFragment extends BaseFragment<LoginViewModel, FragmentLoginBinding, AppRepository, LoginViewModelFactory> {
    LayoutInflater layoutInflater;
    String userId,password;
    LoginRequest loginRequest;
    int melaId;
    boolean status=false;
    AppUtils appUtility;
    String passwordlogin;
    String otpMobileNumber;
    String generatedOTP ="";
    VolleyService volleyService;
    String SEND_OTP_MESSAGE_URL=AppConstants.HTTP_TYPE+"://"+AppConstants.HOST+"/services/melaattend/forgot";
    @Override
    public Class<LoginViewModel> getViewModel() {
        return LoginViewModel.class;
    }
    @Override
    public FragmentLoginBinding getFragmentBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        layoutInflater=inflater;
        return FragmentLoginBinding.inflate(inflater,container,false);
    }
    @Override
    public AppRepository getFragmentRepository() {
        return AppRepository.getInstance(getActivity().getApplication());
    }
    @Override
    public Context getCurrentContext() {
        return getContext();
    }
    @Override
    public LoginViewModelFactory getFactory() {
        return new LoginViewModelFactory(getFragmentRepository());
    }
    @Override
    public void onFragmentReady() {
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        volleyService=VolleyService.getInstance(getCurrentContext());
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              userId=binding.registnNum.getText().toString();
              password=binding.passwrd.getText().toString();
              if(NetworkUtils.isInternetOn(getCurrentContext()))
              {
                  if(userId.isEmpty()||password.isEmpty())
                  {
                      ViewUtilsKt.tost(getCurrentContext(),getCurrentContext().getResources().getString(R.string.user_id_and_password_not_empty));
                  }else {
                      customProgressDialog.showProgress(getCurrentContext().getResources().getString(R.string.loading),false);
                      try {
                        initializationOfModel();
                          JSONObject loginObject=new JSONObject(loginRequest.javaToJson());
                          JSONObject encryptedLoginObject=new JSONObject();
                          try {
                              Cryptography cryptographyLoginObj=new Cryptography();
                              encryptedLoginObject.accumulate("data",cryptographyLoginObj.encrypt(loginObject.toString()));
                          }catch (NoSuchPaddingException e) {
                              e.printStackTrace();
                          } catch (NoSuchAlgorithmException e) {
                              e.printStackTrace();
                          } catch (InvalidKeyException e) {
                              e.printStackTrace();
                          } catch (InvalidAlgorithmParameterException e) {
                              e.printStackTrace();
                          } catch (IllegalBlockSizeException e) {
                              e.printStackTrace();
                          } catch (BadPaddingException e) {
                              e.printStackTrace();
                          } catch (UnsupportedEncodingException e) {
                              e.printStackTrace();
                          }
                          JsonObjectRequest jsonLoginRequest=new JsonObjectRequest(Request.Method.POST, AppConstants.LOGIN_MASTER, encryptedLoginObject, new Response.Listener<JSONObject>() {
                              @Override
                              public void onResponse(JSONObject response) {

                                  AppUtils.getInstance().showLog(""+response.toString(),LoginFragment.class);
                                  insertingDataInDB(response);

                              }
                          }, new Response.ErrorListener() {
                              @Override
                              public void onErrorResponse(VolleyError error) {
                                AppUtils.getInstance().showLog(""+error,LoginFragment.class);
                              }
                          });

                          SingletonVolley.getInstance(getCurrentContext()).addToRequestQueue(jsonLoginRequest);


                      } catch (JSONException e) {
                          e.printStackTrace();
                      }


                  }

              }else
              {
                      AppUtils.getInstance().showLog("Internet is not enable",LoginFragment.class);
                  viewModel.noInterNetConnection(getCurrentContext());
              }
            }
        });
        binding.forgotPasswordTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetPassword();
            }
        });
    }


    public void forgetPassword(){

        MaterialButton cancelBtn,sendOTPBtn;
        TextInputEditText otpMobileEt;
        TextInputLayout mobInputLayout;

        if(NetworkUtils.isInternetOn(getCurrentContext())){
            MaterialAlertDialogBuilder materialAlertDialogBuilder =new MaterialAlertDialogBuilder(getCurrentContext());
            View customLayout = getLayoutInflater().inflate(R.layout.forgot_password_custom_dialog, null);
            materialAlertDialogBuilder.setView(customLayout);
            materialAlertDialogBuilder.setCancelable(false);
            androidx.appcompat.app.AlertDialog cusDialog =materialAlertDialogBuilder.show();

            cancelBtn =customLayout.findViewById(R.id.cancelBtn);
            sendOTPBtn =customLayout.findViewById(R.id.sendOTPBtn);
            mobInputLayout =customLayout.findViewById(R.id.mobInputLayout);
            otpMobileEt =customLayout.findViewById(R.id.otpMobileEt);

            otpMobileEt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mobInputLayout.setError(null);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cusDialog.dismiss();
                }
            });
            sendOTPBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    otpMobileNumber = otpMobileEt.getText().toString().trim();
                    if(otpMobileNumber.isEmpty()||otpMobileNumber.length()<10){
                        mobInputLayout.setError("Enter a Valid Mobile Number");
                    }else {
                        appUtility=AppUtils.getInstance();
                        generatedOTP =appUtility.getRandomOtp();
                       // Toast.makeText(getCurrentContext(),generatedOTP,Toast.LENGTH_LONG).show();
                        callOTPApi();
                        cusDialog.dismiss();
                       /* appSharedPreferences.setMobile(otpMobileNumber);
                        appSharedPreferences.setOtp(generatedOTP);
                        Intent intent =new Intent(LoginActivity.this,OtpVerificationActivity.class);
                        startActivity(intent);*/

                    }
                }
            });

        }else {
            MaterialAlertDialogBuilder materialAlertDialogBuilder =new MaterialAlertDialogBuilder(getCurrentContext());
            materialAlertDialogBuilder.setCancelable(false);
            materialAlertDialogBuilder.setMessage("Please enable your internet...");
            materialAlertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            materialAlertDialogBuilder.show();
        }

    }
    private void callOTPApi() {
        if(NetworkUtils.isInternetOn(getCurrentContext())){
            ProgressDialog progressDialog =new ProgressDialog(getCurrentContext());
            progressDialog.setMessage("loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();
            JSONObject masterUrlObject =new JSONObject();
            try {
               // String msg =generatedOTP+" is One Time Password(OTP) for the Mela Attendance application. Do not share it with anyone.";
                masterUrlObject.accumulate("mobile",otpMobileNumber);
                masterUrlObject.accumulate("melaId",AppConstants.melaId);
                masterUrlObject.accumulate("otpMessage",generatedOTP);

            } catch (JSONException e) {
                e.printStackTrace();
                appUtility.showLog("OTP json making exception:- "+e,LoginActivity.class);
            }

            VolleyResult mResultCallBack = new VolleyResult() {
                @Override
                public void notifySuccess(String requestType, JSONObject response) {
                    progressDialog.dismiss();
                    appUtility.showLog("get response:-" +response,LoginActivity.class);
                    //{"data":"","message":"Mobile Number Invalid","status":0}
                    try{
                        if(response.has("message")){
                            String status =response.getString("message");
                            if(status.equalsIgnoreCase("message send successfully")){
                                ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getOTP(),generatedOTP,getCurrentContext());
                                ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getOtpMobileNumber(),otpMobileNumber,getCurrentContext());
                                Intent intent =new Intent(getCurrentContext(),OtpVerificationActivity.class);
                                startActivity(intent);

                            }else {
                                MaterialAlertDialogBuilder materialAlertDialogBuilder =new MaterialAlertDialogBuilder(getCurrentContext());
                                materialAlertDialogBuilder.setCancelable(false);
                                materialAlertDialogBuilder.setMessage("Mobile Number does not exist...");
                                materialAlertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                                materialAlertDialogBuilder.show();
                            }
                        }
                    }catch (Exception e){
                        appUtility.showLog("OTP get response Expection:-" +e,LoginActivity.class);
                    }
                }

                @Override
                public void notifyError(String requestType, VolleyError error) {
                    progressDialog.dismiss();
                    appUtility.showLog("volley error:-" +error,LoginActivity.class);
                    MaterialAlertDialogBuilder materialAlertDialogBuilder =new MaterialAlertDialogBuilder(getCurrentContext());
                    materialAlertDialogBuilder.setCancelable(false);
                    materialAlertDialogBuilder.setMessage("Server Error please try again");
                    materialAlertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            };
            volleyService.postDataVolley("otp_request",SEND_OTP_MESSAGE_URL,masterUrlObject,mResultCallBack);
        }
        else {
            MaterialAlertDialogBuilder materialAlertDialogBuilder =new MaterialAlertDialogBuilder(getCurrentContext());
            materialAlertDialogBuilder.setCancelable(false);
            materialAlertDialogBuilder.setMessage("Please On Your internet...");
            materialAlertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            materialAlertDialogBuilder.show();
        }

    }


    private void intentToMpin() {
        Intent intent=new Intent(getContext(), MpinActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void insertingDataInDB(JSONObject s) {
        try {
            AppUtils.getInstance().showLog("response"+s,LoginFragment.class);
            Cryptography cryptography = null;
            JSONArray jsonArr=null;
            String objectResponse="";
            if(s.has("data")){
                objectResponse=s.getString("data");
            }else {
                return;
            }
            try {
                cryptography = new Cryptography();
                jsonArr = new JSONArray(cryptography.decrypt(objectResponse));
                AppUtils.getInstance().showLog("LoginResponse"+jsonArr,LoginFragment.class);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            if (jsonArr.toString().equalsIgnoreCase("[]")){
                AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
                builder.setTitle("Info");
                builder.setMessage("No Data Found.");
                builder.setCancelable(false);
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
            }
            JSONArray mainArr = null;
            try {
                mainArr = jsonArr;
            } catch (Exception e) {
                e.printStackTrace();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getCurrentContext());
            for (int i = 0; i < mainArr.length(); i++) {
                JSONObject obj = mainArr.getJSONObject(i);
                if (obj.has("status")) {
                    DialogFactory.getInstance().showServerErrorDialog(getCurrentContext(), obj.getString("status"), "OK");

                }else if(obj.has("login_attempt") || obj.has("reason"))
                {
                    if(i==0) {

                        if(obj.has("login_attempt")) {
                            builder.setTitle(obj.getInt("login_attempt") + " " + "Invalid login attempt");
                            continue;
                        }else{
                            builder.setTitle("Info");
                        }
                    }
                    builder.setMessage(obj.getString("reason"));
                    builder.setCancelable(false);
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    builder.show();
                }

                else {
                    String shgCode = obj.getString("shg_code");
                    int shgRegId = obj.getInt("shg_reg_id");
                    String shgName = obj.getString("shg_name");
                    long loginMobileNumber = obj.getLong("mobile");
                    melaId = obj.getInt("mela_id");
                    String stallNo = obj.getString("stall_no");

                    SHGEntity shgsDetailsOnLoginData = new SHGEntity();

                    if (shgCode != null && !shgCode.isEmpty()) {
                        shgsDetailsOnLoginData.shgCode=shgCode;
                    } else {
                        shgsDetailsOnLoginData.shgCode=AppConstants.NOT_AVAILABLE;
                    }
                    shgsDetailsOnLoginData.shgRegistrationId=shgRegId;

                    if (shgName != null && !shgName.isEmpty()) {
                        shgsDetailsOnLoginData.shgName=shgName;
                    } else {
                        shgsDetailsOnLoginData.shgName=AppConstants.NOT_AVAILABLE;
                    }
                    if (stallNo != null && !stallNo.isEmpty()) {
                        shgsDetailsOnLoginData.stallNo=stallNo;
                    } else {
                        shgsDetailsOnLoginData.stallNo=AppConstants.NOT_AVAILABLE;
                    }
                    shgsDetailsOnLoginData.loginMobilenumber=String.valueOf(loginMobileNumber);
                    shgsDetailsOnLoginData.melaId=melaId;
                  viewModel.insertLoginData(shgsDetailsOnLoginData);
                  //  getFragmentRepository().insertUserData(shgsDetailsOnLoginData);
                }
            }
            customProgressDialog.hideProgress();
        // intentToMpin();

           // password.setVisibility(View.VISIBLE);
            if (viewModel.appRepository.getAllShgData().size()!=0) {
                ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getBundleParticipantMelaidForSetAttendance(), String.valueOf(melaId), getCurrentContext());
                //ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefPhonenoForMpin(), mobileNumber, getCurrentContext());
                AppUtils.getInstance().showLog("password" + password, LoginActivity.class);
                //ProjectPrefrences.getInstance().saveSharedPrefrecesData("password", passwordlogin, getCurrentContext());
                setUpOTPApi();
            }
        } catch (JSONException e) {
            AppUtils.getInstance().showLog("ex"+e,LoginFragment.class);
            status=false;
        }
    }
    private void setUpOTPApi() {
        intentToMpin();
    }
    private void initializationOfModel() {
        loginRequest=new LoginRequest();
        loginRequest.mobileNumber=userId;
        loginRequest.melaId= AppConstants.melaId;
        loginRequest.password=password;
        //loginRequest.deviceInfo= AppDeviceInfoUtils.getInstance(getCurrentContext()).getDeviceInfo();
        loginRequest.deviceInfo= "Xiaomi-dandelion-M2006C3LI";
       // loginRequest.imei=AppDeviceInfoUtils.getInstance(getCurrentContext()).getIMEINo1();
        loginRequest.imei="0c09a4494a7992e2";
        loginRequest.appLoginTime= AppDateFactory.getInstance().getDateTime();
        ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyShgMobile(),userId,getCurrentContext());
        ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyImei(),"abc",getCurrentContext());
        ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyDeviceInfo(),AppDeviceInfoUtils.getInstance(getCurrentContext()).getDeviceInfo(),getCurrentContext());
    }
}
