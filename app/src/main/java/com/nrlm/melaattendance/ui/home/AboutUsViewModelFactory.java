package com.nrlm.melaattendance.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;
import com.nrlm.melaattendance.utils.BaseFragment;

public class AboutUsViewModelFactory implements ViewModelProvider.Factory {
    BaseRepository baseRepository;
    public  AboutUsViewModelFactory(BaseRepository baseRepository)

    {
        this.baseRepository=baseRepository;

    }



    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        T  aboutUsViewModel=null;
        if (modelClass.isAssignableFrom(AboutUsViewModel.class)) {
            aboutUsViewModel = (T) new AboutUsViewModel((AppRepository) baseRepository);
        }
        return aboutUsViewModel;
    }
}
