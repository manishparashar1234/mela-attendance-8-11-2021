package com.nrlm.melaattendance.database.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.nrlm.melaattendance.database.entity.SHGEntity;

import java.util.List;

@Dao
public interface SHGDao {
    @Insert
void insertAll(SHGEntity shgEntity);

@Query("SELECT * from SHGEntity")
    List<SHGEntity> getShgAllData();

@Query("DELETE FROM SHGEntity")
void deleteTable();
}
