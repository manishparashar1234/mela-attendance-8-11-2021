package com.nrlm.melaattendance.ui.login;

import android.content.Context;

import androidx.lifecycle.ViewModel;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.database.AppDatabase;
import com.nrlm.melaattendance.database.entity.SHGEntity;
import com.nrlm.melaattendance.repository.AppRepository;

public class LoginViewModel extends ViewModel {
    public AppRepository appRepository;


    public void insertLoginData(SHGEntity loginResponse) {
        appRepository.insertUserData(loginResponse);

    }
    public LoginViewModel(AppRepository appRepository) {
        this.appRepository = appRepository;
    }
    public void noInterNetConnection(Context context){
        new MaterialAlertDialogBuilder(context).setTitle(context.getResources().getString(R.string.dialog_network_msg_title)).setIcon(R.drawable.ic_baseline_signal_wifi_connected_no_internet)
                .setMessage(context.getResources().getString(R.string.dialog_network_msg))
                .setPositiveButton(context.getResources().getString(R.string.dialog_positive_btn),(dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }).setNegativeButton(context.getResources().getString(R.string.dialog_cancel_btn),(dialogInterface, i) -> {
            dialogInterface.dismiss();
        }).show();
    }
}
