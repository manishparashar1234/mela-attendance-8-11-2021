package com.nrlm.melaattendance.utils;


public class AppConstants {
    public static final String melaId="10"+"";
    public static final String NOT_AVAILABLE = "Not Available";

//For local
/*
    public static final String HTTP_TYPE="http";
*/
/*
    public static final String HOST="10.197.183.105:8080/nrlmwebservice";
*/

    //For Live Server
   public static final String HTTP_TYPE="https";
   public static final String HOST="nrlm.gov.in/nrlmwebservice";
   //For Demo Server
/*   public static final String HTTP_TYPE="https";
   public static final String HOST="nrlm.gov.in/nrlmwebservicedemo";*/

    public static final String LOGIN_MASTER=HTTP_TYPE+"://"+HOST+"/services/mela/users";
}
