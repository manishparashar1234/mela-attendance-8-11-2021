package com.nrlm.melaattendance.ui.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.compose.runtime.external.kotlinx.collections.immutable.implementations.immutableList.UtilsKt;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.NavGraph;
import androidx.navigation.NavInflater;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.navigation.NavigationView;
import com.nrlm.melaattendance.MainActivity;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.adapter.SelectShgAdapter;
import com.nrlm.melaattendance.database.entity.SHGEntity;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.ui.login.LoginActivity;
import com.nrlm.melaattendance.ui.mpin.MpinActivity;
import com.nrlm.melaattendance.utils.AppUtils;
import com.nrlm.melaattendance.utils.GPSTracker;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ViewUtilsKt;

import java.util.ArrayList;
import java.util.List;




public class HomeActivity extends AppCompatActivity  {
    // List<SHGEntity> shgData;
    // AppRepository appRepository;
    // RecyclerView selectRecy;
    String latitude,longitude;
    BottomNavigationView bottomNavigationView;
    NavController navController;
    NavInflater navInflater;
    NavGraph navGraph;
    AlertDialog alertDialog;
    public OnBackPressedListener onBackPressedListener;
int count=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getFreshLocation();
        this.bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.home_nav_host);
        navController = navHostFragment.getNavController();
        navInflater=navController.getNavInflater();
        navGraph=navInflater.inflate(R.navigation.home_nave_graph);



        getHomefragment();

        //    selectRecy = (RecyclerView) findViewById(R.id.rvSelectShg);
        //  showingShgDataOverRecy();  //This method is used to showing the data over the adapter
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.about:
                        getAboutFragment();
                        AppUtils.getInstance().showLog("Welcome to profile", HomeActivity.class);
                        break;
                    case R.id.logout:
                        showingLogoutDialog();


                        break;
                    case R.id.contact_us:
                        getContactUsFragment();
                        AppUtils.getInstance().showLog("Welcome to contact us", HomeActivity.class);
                        break;
                    case R.id.home:
                        getHomefragment();
                        AppUtils.getInstance().showLog("Welcome to home", HomeActivity.class);
                        break;
                }

                return true;
            }
        });
    }

    private void getFreshLocation()
    {
        GPSTracker gpsTracker = new GPSTracker((Activity)HomeActivity.this);
        gpsTracker.getLocation();
        latitude = String.valueOf(gpsTracker.latitude);
        longitude = String.valueOf(gpsTracker.longitude);
        //  AddTrainingPojo.addTrainingPojo.setGpsLoation(latitude + "lat"+"," + longitude+"long");
        AppUtils.getInstance().showLog("location" + latitude + "  " + longitude, SelectShgAdapter.class);

    }

    @Override
    public void onBackPressed() {

if(count>1) {
    alertDialog = new MaterialAlertDialogBuilder(HomeActivity.this).setIcon(R.drawable.ic_baseline_cancel)
            .setTitle("Alert").setMessage("Do you want to exit to the app.")
            .setCancelable(false)
            .setPositiveButton("Ok", (dialogInterface, i) -> {
                dialogInterface.dismiss();
                onBackPressedListener.doBack();
                System.exit(0);
            }).setNegativeButton(HomeActivity.this.getResources().getString(R.string.dialog_cancel_btn), (dialogInterface, i) -> {
                dialogInterface.dismiss();
            }).show();
}else {
    bottomNavigationView.setSelectedItemId(R.id.home);
}
count++;
    }
    public interface OnBackPressedListener {
        void doBack();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

     private void showingLogoutDialog(){
        alertDialog = new MaterialAlertDialogBuilder(HomeActivity.this).setIcon(R.drawable.ic_baseline_logout)
                .setTitle(HomeActivity.this.getResources().getString(R.string.dialog_sign_out_title)).setMessage(HomeActivity.this.getResources().getString(R.string.dialog_sign_out_msg))
                .setCancelable(false)
                .setPositiveButton(HomeActivity.this.getResources().getString(R.string.dialog_btn_signout), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    deleteTableFromDB();
                    clearAllValueFromSharedPrefrance();
                    //  removeDataAtLogout();
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    HomeActivity.this.startActivity(intent);
                    HomeActivity.this.finish();

                }).setNegativeButton(HomeActivity.this.getResources().getString(R.string.dialog_cancel_btn), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }).show();
    }
    private void deleteTableFromDB()
    {
        AppRepository.getInstance(getApplication()).deleteTables();
    }
    private void clearAllValueFromSharedPrefrance()
    {

        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleUsername(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getShgRegistrationId(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyShgMobile(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyImei(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyDeviceInfo(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyDate(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyInTime(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyInAddress(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyOutTime(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyLoginDone(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyNextDayDate(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getPrefKeyOutAddress(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleAttedenceClosingLocation(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleAttedenceLastClosingTime(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleAttedenceMobileNumber(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleAttedenceOpeningLocation(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleAttedenceOpeningTime(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleAttedenceStatus(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleParticipantMelaidForSetAttendance(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getBundleParticipantShgregidForSetAttendance(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getOTP(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getOtpMobileNumber(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getParticipantMobile(),HomeActivity.this);
        PrefrenceManager.getInstance().removeSharedPrefrencesData(PrefrenceManager.getProductIdForBarcode(),HomeActivity.this);

    }
    private void getAboutFragment()
    {
        navGraph.setStartDestination(R.id.aboutUsFragment);
        navController.setGraph(navGraph);
    }
private void getHomefragment()
{
    navGraph.setStartDestination(R.id.homeFragment);
    navController.setGraph(navGraph);
}
private void getContactUsFragment()
{
navGraph.setStartDestination(R.id.contactUsFragment);
navController.setGraph(navGraph);
}
    private void showingShgDataOverRecy() {
      /* getingShgDataFromDB();
        SelectShgAdapter selectShgAdapter=new SelectShgAdapter(HomeActivity.this,shgData);
             selectRecy.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
             selectRecy.setAdapter(selectShgAdapter);
             selectShgAdapter.notifyDataSetChanged();
            }
  private void  getingShgDataFromDB()
    {
        appRepository=AppRepository.getInstance(getApplication());
        shgData=new ArrayList<>();
        shgData=appRepository.getAllShgData();
        if(shgData.size()==0)
        {
            AppUtils.getInstance().showLog("List is empty",HomeActivity.class);
        }
    }*/

    }
}



