package com.nrlm.melaattendance.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nrlm.melaattendance.BuildConfig;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.ui.login.LoginActivity;
import com.nrlm.melaattendance.ui.mpin.MpinActivity;
import com.nrlm.melaattendance.utils.AppConstants;
import com.nrlm.melaattendance.utils.AppUtils;
import com.nrlm.melaattendance.utils.Cryptography;
import com.nrlm.melaattendance.utils.DialogFactory;
import com.nrlm.melaattendance.utils.NetworkUtils;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ProjectPrefrences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class
SplashActivity extends AppCompatActivity {
    private static int SPLASH_SCREEN_TIME_OUT=4000;
    private String response=null,status;                      //status tells about the closing and opening of the mela
    String login_status,version;
    private Context context;
    public static final int RequestPermissionCode = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getVersionStatus();
    }

    private void getVersionStatus() {
        if (!NetworkUtils.isInternetOn(SplashActivity.this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
            builder.setTitle("No Internet!");
            builder.setMessage("Please open your Internet Connection.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    finish();
                }
            });
            builder.show();
        }else {
            String url = AppConstants.HTTP_TYPE+"://"+AppConstants.HOST+"/services/mela/version";
            JSONObject versionPayload=new JSONObject();
            try {
                versionPayload.accumulate("version", BuildConfig.VERSION_NAME);
                versionPayload.accumulate("melaId",AppConstants.melaId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            JSONObject encryptedObject =new JSONObject();
            try {
                Cryptography cryptography = new Cryptography();
                String encrypted=cryptography.encrypt(versionPayload.toString());
                encryptedObject.accumulate("data",encrypted);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (InvalidAlgorithmParameterException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            JsonObjectRequest versionRequest = new JsonObjectRequest(Request.Method.POST, url, encryptedObject,new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject s) {
                    //  response=s;
                    Cryptography cryptography = null;
                    String versionString="";
                    String objectResponse="";
                    if(s.has("data")){
                        try {
                            objectResponse=s.getString("data");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {
                        return;
                    }
                    try {
                        cryptography = new Cryptography();
                        versionString=cryptography.decrypt(objectResponse);
                            Log.d("TAG"+s,"data_value");
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    responseParshing(versionString);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    DialogFactory.getInstance().showAlert(SplashActivity.this,"Sorry we are facing some server issue.","Ok");

                       Log.d("TAG", "error" + error);
                }
            });
      /*      stringRequest.setRetryPolicy(new RetryPolicy() {

                @Override
                public int getCurrentTimeout() {
                    return DefaultRetryPolicy.DEFAULT_TIMEOUT_MS;
                }

                @Override
                public int getCurrentRetryCount() {
                    return DefaultRetryPolicy.DEFAULT_MAX_RETRIES;
                }

                @Override
                public void retry(VolleyError error) throws VolleyError {
                    if (NetworkFactory.isInternetOn(SplashScreen.this)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this);
                        builder.setTitle("Poor Internet!");
                        builder.setMessage("Please connect your phone with high speed Internet Connection" + error);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                finish();
                            }
                        });
                        builder.show();
                    }
                }
            });*/
            RequestQueue requestQueue = Volley.newRequestQueue(SplashActivity.this);
            requestQueue.add(versionRequest);
        }
    }

    private void  responseParshing(String response){
        try {
            JSONArray mainArr = new JSONArray(response);
           if( mainArr.toString().equalsIgnoreCase("[]"))
            {
                DialogFactory.getInstance().showAlert(SplashActivity.this,"Sorry we are facing some server issue.","Ok");
                return;
            }

            for (int i = 0; i < mainArr.length(); i++) {
                JSONObject obj = mainArr.getJSONObject(i);
                version = obj.getString("version");
                if(version.equalsIgnoreCase("correct")) {             //because at incorrect version not receivinng the status of mela
                    status = obj.getString("status");
                    if(status.equalsIgnoreCase("open")) {
                        PrefrenceManager.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyOriginLet(), obj.getString("latitude"), SplashActivity.this);
                        PrefrenceManager.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyOriginLong(), obj.getString("longitude"), SplashActivity.this);
                        PrefrenceManager.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefRadius(), obj.getString("radius"), SplashActivity.this);
                    }
                }
            }
            AppUtils.getInstance().showLog(version, SplashActivity.class);

            if (version.equalsIgnoreCase("correct")) {
                if(status.equalsIgnoreCase("open")) {
                    loadNextScreenWithDelay();
                }else
                {
                    showDialogForMelaClose();
                }

            } else {
                showDialogForVersion();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void loadNextScreenWithDelay() {
        android.os.Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                moveToNextAndClearPreviousScreens(LoginActivity.class);
            }
        }, 3000);
    }
    private void showDialogForMelaClose()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle(getString(R.string.alert));
        builder.setMessage(getString(R.string.mela_close_msg));
        builder.setCancelable(false);
        builder.setPositiveButton("Yes" ,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }
    private void moveToNextAndClearPreviousScreens(Class<LoginActivity> loginActivityClass) {
        login_status = ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyLoginDone(), SplashActivity.this);
        if (login_status.equalsIgnoreCase("ok")) {
            Intent intent = new Intent(SplashActivity.this, MpinActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            clearingAllTableOfLocalDB();
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
    }

    private void clearingAllTableOfLocalDB() {
        AppRepository.getInstance(getApplication()).deleteTables();
    }

    private void showDialogForVersion() {
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle(getString(R.string.update_application_title));
        builder.setMessage(R.string.update_application_msg);
        builder.setCancelable(false);
        builder.setPositiveButton("Yes" ,new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateApplication();
            }
        });
        builder.show();

    }
    private void updateApplication() {
        String nrlmLiveLocUri = "https://drive.google.com/drive/folders/1LjPskVPqontHuZRTKdqH1kcGTxwsI__C?usp=sharing";

        try {
            SplashActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(nrlmLiveLocUri)));
        } catch (android.content.ActivityNotFoundException anfe) {
            //((Activity) context).startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            SplashActivity.this.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(nrlmLiveLocUri)));
        }
        ((Activity) SplashActivity.this).finish();
    }


}