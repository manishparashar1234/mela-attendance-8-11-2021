package com.nrlm.melaattendance.ui.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.ui.login.LoginActivity;
import com.nrlm.melaattendance.utils.AppDateFactory;
import com.nrlm.melaattendance.utils.AppUtils;
import com.nrlm.melaattendance.utils.CustomProgressDialog;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ViewUtilsKt;

public class LogoutDialog extends DialogFragment {
    AlertDialog alertDialog;
    CustomProgressDialog customProgressDialog;
    AppRepository appRepository;
    AppUtils appUtils;
    //AppSharedPreferences appSharedPreferences;
    AppDateFactory appDateFactory;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        customProgressDialog = CustomProgressDialog.newInstance(requireContext());
        appRepository = AppRepository.getInstance(getActivity().getApplication());
        appUtils = AppUtils.getInstance();
        appDateFactory = AppDateFactory.getInstance();
        // appSharedPreferences=AppSharedPreferences.getInstance(requireContext());
        alertDialog = new MaterialAlertDialogBuilder(requireContext()).setIcon(R.drawable.ic_baseline_logout)
                .setTitle(getContext().getResources().getString(R.string.dialog_sign_out_title)).setMessage(getContext().getResources().getString(R.string.dialog_sign_out_msg))
                .setCancelable(false)
                .setPositiveButton(getContext().getResources().getString(R.string.dialog_btn_signout), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    ViewUtilsKt.tost(requireContext(), getContext().getResources().getString(R.string.toast_success_sign_out));
                    appRepository.deleteTables();
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getContext().startActivity(intent);
                    getActivity().finish();
                }).setNegativeButton(getContext().getResources().getString(R.string.dialog_cancel_btn), (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                }).show();
        setCancelable(false);
        return alertDialog;
    }
}