package com.nrlm.melaattendance.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.nrlm.melaattendance.database.dao.AttendanceFlagDataDao;
import com.nrlm.melaattendance.database.dao.SHGDao;
import com.nrlm.melaattendance.database.entity.AttendanceFlagDataEntity;
import com.nrlm.melaattendance.database.entity.SHGEntity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {SHGEntity.class, AttendanceFlagDataEntity.class},version = 1,exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {
public static final String DATABASE_NAME="melaAttendance.db";
public static volatile AppDatabase instance;
private static final int NUMBER_OF_THREADS = 5;
public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

public abstract SHGDao shgDao();
public abstract AttendanceFlagDataDao attendanceFlagDataDao();

    public static AppDatabase getDatabase(final Context context) {
        if (instance == null) {
            synchronized (AppDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DATABASE_NAME).fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return instance;
    }
}
