package com.nrlm.melaattendance.model.Request;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRequest implements Serializable {
    @SerializedName("mobile")
    public String mobileNumber;
    @SerializedName("melaId")
    public String melaId;
    @SerializedName("password")
    public String password;
    @SerializedName("deviceInfo")
    public String deviceInfo;
    @SerializedName("imei")
    public String imei;
    @SerializedName("app_login_time")
    public String appLoginTime;

    public static LoginRequest jsonToJava(String jsonString) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, LoginRequest.class);
    }

    public String javaToJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
