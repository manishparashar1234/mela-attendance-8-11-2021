package com.nrlm.melaattendance.utils;

public class PrefrenceManager {
    public static ProjectPrefrences prefrenceManager=null;
    private static final String PREF_KEY_MPIN= "prfMPIN";
    private static final String PREF_KEY_SHG_MOBILE= "shgMobile";
    private static final String PREF_KEY_IMEI="imei";
    private static final String PREF_KEY_DEVICE_INFO="deviceInfo";
    private static final String BUNDLE_USERNAME="userName";
    private static final String SHG_REGISTRATION_ID="shgRegId";
    //key for get attedence detail from server
    private static final String BUNDLE_ATTEDENCE_OPENING_LOCATION="openingLocation";
    private static final String BUNDLE_ATTEDENCE_OPENING_TIME="openingTime";
    private static final String BUNDLE_ATTEDENCE_LAST_CLOSING_TIME="lastClosingTime";
    private static final String BUNDLE_ATTEDENCE_CLOSING_LOCATION="closingLocation";
    private static final String BUNDLE_ATTEDENCE_STATUS="attedenceStatus";
    private static final String BUNDLE_ATTEDENCE_MOBILE_NUMBER="attedenceMobile";
    private  static final String PRODUCT_ID_FOR_BARCODE="pdctIdBarcode";
    private static final String OTP_MOBILE_NUMBER="otpmobilenumber";
    private static final String OTP="otp";
    private static final String BUNDLE_PARTICIPANT_STATUS_FOR_SET_ATTENDANCE="participantStatus";
    private static final String PARTICIPANT_MOBILE="pm";
    private static final String BUNDLE_PARTICIPANT_SHGREGID_FOR_SET_ATTENDANCE="bundleParticipantShgRegId";
/********************** Activity attendance******************/
    private static final String PREF_KEY_IN_TIME = "inTime";
    private static final String PREF_KEY_DATE = "inDate";
    private static final String PREF_KEY_OUT_TIME = "outTime";
    private static final String PREF_KEY_IN_ADDRESS = "inAddress";
    private static final String PREF_KEY_OUT_ADDRESS = "outAddress";
    private static final String PREF_KEY_NEXT_DAY_DATE= "nextDayDate";
    private static final String BUNDLE_PARTICIPANT_MELAID_FOR_SET_ATTENDANCE="bundleParticipantMelaID";
    private static final String PREF_KEY_LOGIN_DONE = "Longin";
    private static final String PREF_KEY_ORIGIN_LET="originLat";
    private static final String PREF_KEY_ORIGIN_LONG="originLong";
    private static final String PREF_RADIUS="radius";

    public static String getPrefRadius() {
        return PREF_RADIUS;
    }



    public static ProjectPrefrences getInstance()
    {
        if(prefrenceManager==null)
        {
            prefrenceManager=new ProjectPrefrences();
        }
        return  prefrenceManager;
    }

    public static String getPrefKeyOriginLet() {
        return PREF_KEY_ORIGIN_LET;
    }

    public static String getPrefKeyOriginLong() {
        return PREF_KEY_ORIGIN_LONG;
    }



    public static String getPrefKeyLoginDone() {
        return PREF_KEY_LOGIN_DONE;
    }
    public static String getBundleParticipantMelaidForSetAttendance() {
        return BUNDLE_PARTICIPANT_MELAID_FOR_SET_ATTENDANCE;

    }
    public static String getPrefKeyNextDayDate() {
        return PREF_KEY_NEXT_DAY_DATE;
    }
    public static String getPrefKeyInTime() {
        return PREF_KEY_IN_TIME;
    }

    public static String getPrefKeyDate() {
        return PREF_KEY_DATE;
    }

    public static String getPrefKeyOutTime() {
        return PREF_KEY_OUT_TIME;
    }

    public static String getPrefKeyInAddress() {
        return PREF_KEY_IN_ADDRESS;
    }

    public static String getPrefKeyOutAddress() {
        return PREF_KEY_OUT_ADDRESS;
    }
    public static String getBundleParticipantShgregidForSetAttendance() {
        return BUNDLE_PARTICIPANT_SHGREGID_FOR_SET_ATTENDANCE;
    }
    public static String getBundleParticipantStatusForSetAttendance() {
        return BUNDLE_PARTICIPANT_STATUS_FOR_SET_ATTENDANCE;
    }

    public static String getParticipantMobile() {
        return PARTICIPANT_MOBILE;
    }
    public static String getBundleAttedenceOpeningLocation() {
        return BUNDLE_ATTEDENCE_OPENING_LOCATION;
    }

    public static String getBundleAttedenceOpeningTime() {
        return BUNDLE_ATTEDENCE_OPENING_TIME;
    }

    public static String getBundleAttedenceLastClosingTime() {
        return BUNDLE_ATTEDENCE_LAST_CLOSING_TIME;
    }

    public static String getBundleAttedenceClosingLocation() {
        return BUNDLE_ATTEDENCE_CLOSING_LOCATION;
    }

    public static String getBundleAttedenceStatus() {
        return BUNDLE_ATTEDENCE_STATUS;
    }

    public static String getBundleAttedenceMobileNumber() {
        return BUNDLE_ATTEDENCE_MOBILE_NUMBER;
    }
    public static String getProductIdForBarcode() {
        return PRODUCT_ID_FOR_BARCODE;
    }
    public static String getOtpMobileNumber() {
        return OTP_MOBILE_NUMBER;
    }

    public static String getOTP() {
        return OTP;
    }
    public static String getBundleUsername() {
        return BUNDLE_USERNAME;
    }

    public static String getShgRegistrationId() {
        return SHG_REGISTRATION_ID;
    }
    public static String getPrefKeyImei() {
        return PREF_KEY_IMEI;
    }
    public static String getPrefKeyDeviceInfo() {
        return PREF_KEY_DEVICE_INFO;
    }
    public static String getPrefKeyShgMobile() {
        return PREF_KEY_SHG_MOBILE;
    }
    public static String getPrefKeyMpin() {
        return PREF_KEY_MPIN;
    }

}
