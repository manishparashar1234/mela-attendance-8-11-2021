package com.nrlm.melaattendance.ui.mpin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.databinding.FragmentVerifyMpinBinding;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.ui.home.HomeActivity;
import com.nrlm.melaattendance.utils.BaseFragment;
import com.nrlm.melaattendance.utils.DialogFactory;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ViewUtilsKt;

public class VerifyMpinFragment extends BaseFragment<MpinViewModel, FragmentVerifyMpinBinding, AppRepository,MpinViewModelFactory>
{
    @Override
    public Class<MpinViewModel> getViewModel() {
        return MpinViewModel.class;
    }

    @Override
    public FragmentVerifyMpinBinding getFragmentBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        return FragmentVerifyMpinBinding.inflate(inflater,container,false);
    }

    @Override
    public AppRepository getFragmentRepository() {
        return AppRepository.getInstance(getActivity().getApplication());
    }

    @Override
    public Context getCurrentContext() {
        return getContext();
    }

    @Override
    public MpinViewModelFactory getFactory() {
        return new MpinViewModelFactory(getFragmentRepository());
    }

    @Override
    public void onFragmentReady() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         binding.btnVerify.setOnClickListener(viewBtn->{
          String vrfMpin=binding.pinviewGetMpin.getText().toString();
           if(vrfMpin.isEmpty())
           {
               ViewUtilsKt.tost(getCurrentContext(),getCurrentContext().getResources().getString(R.string.mpin_can_not_empty));
           }else{
              if(vrfMpin.equalsIgnoreCase(PrefrenceManager.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyMpin(),getCurrentContext()))){
                  PrefrenceManager.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyLoginDone(),"ok",getCurrentContext());
                  Intent intentToHomeActy=new Intent(getCurrentContext(), HomeActivity.class);
                  intentToHomeActy.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                   startActivity(intentToHomeActy);
              }else
              {
                  DialogFactory.getInstance().showAlert(getCurrentContext(),"Entered Mpin is wrong.","Ok");
              }
           }
         });
    }
}
