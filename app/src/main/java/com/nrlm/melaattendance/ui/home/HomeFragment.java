package com.nrlm.melaattendance.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.adapter.SelectShgAdapter;
import com.nrlm.melaattendance.database.entity.SHGEntity;
import com.nrlm.melaattendance.databinding.FragmentHomeBinding;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;
import com.nrlm.melaattendance.utils.AppUtils;
import com.nrlm.melaattendance.utils.BaseFragment;
import com.nrlm.melaattendance.utils.GPSTracker;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ProjectPrefrences;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends BaseFragment<HomeViewModel, FragmentHomeBinding, AppRepository,HomeViewModelFactory> {
    LayoutInflater layoutInflater;
    List<SHGEntity> shgData;
       RecyclerView selectRecy;
    SelectShgAdapter selectShgAdapter;
    String latitude,longitude;
    private double originLat , originLon ; // change the let long of the origin
    private int disInMtr;
    @Override
    public Class<HomeViewModel> getViewModel() {
        return HomeViewModel.class;
    }

    @Override
    public FragmentHomeBinding getFragmentBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        layoutInflater=inflater;
        setHasOptionsMenu(true);
        return FragmentHomeBinding.inflate(inflater,container,false);
    }

    @Override
    public AppRepository getFragmentRepository() {
        return AppRepository.getInstance(getActivity().getApplication());
    }

    @Override
    public Context getCurrentContext() {
         return getContext();
    }

    @Override
    public HomeViewModelFactory getFactory() {
        return new HomeViewModelFactory(getFragmentRepository());
    }

    @Override
    public void onFragmentReady() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        selectRecy = (RecyclerView) view.findViewById(R.id.rvSelectShg);
        getFreshLocation();
        showingShgDataOverRecy();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem item = menu.findItem(R.id.search_menu);
        item.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItem.SHOW_AS_ACTION_IF_ROOM);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setQueryHint("Type SHG name");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Here is where we are going to implement the filter logic
                selectShgAdapter.getFilter().filter(newText);
                return true;
            }

        });
        super.onCreateOptionsMenu(menu,inflater);
    }
    private void getFreshLocation()
    {
        GPSTracker gpsTracker = new GPSTracker((Activity)getCurrentContext());
        gpsTracker.getLocation();
        latitude = String.valueOf(gpsTracker.latitude);
        longitude = String.valueOf(gpsTracker.longitude);
        //  AddTrainingPojo.addTrainingPojo.setGpsLoation(latitude + "lat"+"," + longitude+"long");
        AppUtils.getInstance().showLog("location" + latitude + "  " + longitude, SelectShgAdapter.class);

    }
    private void showingShgDataOverRecy() {
        getingShgDataFromDB();
        originLat= Double.parseDouble(ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyOriginLet(),getCurrentContext()));
        originLon=Double.parseDouble(ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyOriginLong(),getCurrentContext()));
        disInMtr= Integer.parseInt(ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefRadius(),getCurrentContext()));
         selectShgAdapter=new SelectShgAdapter(getCurrentContext(),shgData,originLat,originLon,disInMtr);
        selectRecy.setLayoutManager(new LinearLayoutManager(getCurrentContext()));
        selectRecy.setAdapter(selectShgAdapter);
        selectShgAdapter.notifyDataSetChanged();
    }
    private void  getingShgDataFromDB()
    {

        shgData=new ArrayList<>();
        shgData=getFragmentRepository().getAllShgData();
        if(shgData.size()==0)
        {
            AppUtils.getInstance().showLog("List is empty",HomeActivity.class);
        }
    }
}
