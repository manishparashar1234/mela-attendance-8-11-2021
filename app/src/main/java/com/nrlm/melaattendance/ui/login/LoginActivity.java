package com.nrlm.melaattendance.ui.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.utils.PermissionHelper;

public class LoginActivity extends AppCompatActivity {
    PermissionHelper permissionHelper;
    boolean checkPermisson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        permissionHelper=PermissionHelper.getInstance(LoginActivity.this);
        checkPermisson=permissionHelper.checkAndRequestPermissions();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        permissionHelper.requestPermissionResult(PermissionHelper.REQUEST_ID_MULTIPLE_PERMISSIONS,permissions,grantResults);
        checkPermisson=true;
    }
}
