package com.nrlm.melaattendance.repository;

import android.app.Application;

import com.nrlm.melaattendance.database.AppDatabase;
import com.nrlm.melaattendance.database.dao.AttendanceFlagDataDao;
import com.nrlm.melaattendance.database.dao.SHGDao;

public abstract class  BaseRepository {
    public Application application;
public SHGDao shgDao;
public AttendanceFlagDataDao attendanceFlagDataDao;
public AppDatabase appDatabase;
    public BaseRepository(Application application) {
        this.application = application;
    }

    public void getAllInstance() {
        appDatabase = AppDatabase.getDatabase(application.getApplicationContext());
        shgDao=appDatabase.shgDao();
        attendanceFlagDataDao=appDatabase.attendanceFlagDataDao();

    }
    public void deleteTables(){

        AppDatabase.databaseWriteExecutor.execute(() -> {
     shgDao.deleteTable();
        });


        /*****assessment ,yesNo, noOperationReason,langdao,monthlyTrackingDao*****/
    }

}
