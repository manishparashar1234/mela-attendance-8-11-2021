package com.nrlm.melaattendance.ui.mpin;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.NavGraph;
import androidx.navigation.NavInflater;

import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.databinding.FragmentSetMpinBinding;
import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;
import com.nrlm.melaattendance.utils.BaseFragment;
import com.nrlm.melaattendance.utils.DialogFactory;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ViewUtilsKt;

public class SetMpinFragment extends BaseFragment<MpinViewModel, FragmentSetMpinBinding, AppRepository,MpinViewModelFactory> {
    @Override
    public Class<MpinViewModel> getViewModel() {
        return MpinViewModel.class;
    }

    @Override
    public FragmentSetMpinBinding getFragmentBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        return FragmentSetMpinBinding.inflate(inflater,container,false);
    }

    @Override
    public AppRepository getFragmentRepository() {
        return AppRepository.getInstance(getActivity().getApplication());
    }

    @Override
    public Context getCurrentContext() {
        return getContext();
    }

    @Override
    public MpinViewModelFactory getFactory() {
        return new MpinViewModelFactory(getFragmentRepository());
    }

    @Override
    public void onFragmentReady() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnSetMpin.setOnClickListener(viewBtn->{
             String mpin=binding.pinviewFirst.getText().toString();
             String verifyMpin=binding.pinviewSecond.getText().toString();
             if(mpin.isEmpty())
             {
                 ViewUtilsKt.tost(getCurrentContext(),getCurrentContext().getResources().getString(R.string.enter_mpin_first));
             }else if(verifyMpin.isEmpty()){
                   ViewUtilsKt.tost(getCurrentContext(),getCurrentContext().getResources().getString(R.string.enter_confirm_first));
            }else
             {
                 if(mpin.equalsIgnoreCase(verifyMpin))
                 {
                     PrefrenceManager.getInstance().saveSharedPrefrecesData(PrefrenceManager.getPrefKeyMpin(),verifyMpin,getCurrentContext());
                     NavDirections navDirections= SetMpinFragmentDirections.actionSetMpinFragmentToVerifyMpinFragment();
                     navController.navigate(navDirections);
                 }else{
                     DialogFactory.getInstance().showAlert(getCurrentContext(),"Entered Mpin is wrong.","Ok");

                 }

             }

        });

    }
}
