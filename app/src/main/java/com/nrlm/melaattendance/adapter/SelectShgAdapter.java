package com.nrlm.melaattendance.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.nrlm.melaattendance.R;
import com.nrlm.melaattendance.database.entity.SHGEntity;
import com.nrlm.melaattendance.model.pojo.ShgData;
import com.nrlm.melaattendance.utils.AppConstants;
import com.nrlm.melaattendance.utils.AppUtils;
import com.nrlm.melaattendance.utils.Cryptography;
import com.nrlm.melaattendance.utils.DialogFactory;
import com.nrlm.melaattendance.utils.GPSTracker;
import com.nrlm.melaattendance.utils.NetworkUtils;
import com.nrlm.melaattendance.utils.PrefrenceManager;
import com.nrlm.melaattendance.utils.ProjectPrefrences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class SelectShgAdapter extends RecyclerView.Adapter<SelectShgAdapter.MyViewHolder> implements Filterable {
    private double originLat , originLon; // change the let long of the origin
    private int disInMtr;
    boolean isWithinRange;
    Context context;
    List<SHGEntity> shgData;
    List<SHGEntity> shgDetailForSearch;              //List which is use to filter the SHG
    List<Object> selectAttedenceTypes=new ArrayList();
    int melaId,shgRegId;
    String latitude="";
    String longitude="";
    String mobileNumber,shgName;
    String userName,stallNo;
    ProgressDialog progressDialog;
    RecyclerView selectAttedenceRV;



    public SelectShgAdapter(Context context, List<SHGEntity> shgData,double originLat,double originLon,int disInMtr )
    {
        this.context=context;
        this.shgData=shgData;
        this.shgDetailForSearch=new ArrayList<>(shgData);
        this.originLat=originLat;
        this.originLon=originLon;
        this.disInMtr=disInMtr;
    }

    @NonNull
    @Override
    public SelectShgAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_select_shg_layout,parent,false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull SelectShgAdapter.MyViewHolder holder, int position) {
        holder.shgName.setText(shgData.get(position).shgName);
        holder.shgCode.setText(shgData.get(position).shgCode);
        holder.shgMobile.setText(shgData.get(position).loginMobilenumber);
        /*holder.shgRegId.setText(""+shgsDetailsOnLoginDataList.get(position).getShgRegistrationId());*/
        holder.shgStallno.setText(shgData.get(position).stallNo);
        getLocation();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!NetworkUtils.isInternetOn(context)) {
                     AppUtils.getInstance().noInterNetConnection(context);

                }else {
                    getFreshLocation();
                }
                checkingTheRadius();
                progressDialog = DialogFactory.getInstance().showProgressDialog(context);
                progressDialog.show();
                selectAttedenceTypes.clear();
                shgRegId = shgData.get(position).shgRegistrationId;
                AppUtils.getInstance().showLog("shgRegId" + shgRegId, SelectShgAdapter.class);
                mobileNumber = shgData.get(position).loginMobilenumber;
                shgName = shgData.get(position).shgName;
                stallNo = shgData.get(position).stallNo;
                saveInPref(shgRegId, shgName);
                //memberDataRequest();

                if(isWithinRange) {
                    memberDataRequest();
                }else {
                    DialogFactory.getInstance().showAlert(context,"You might outside the region or haven't opened your location.","Ok");
                    Toast.makeText(context, "Outside", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }
        });

    }
private void getFreshLocation()
{
    GPSTracker gpsTracker = new GPSTracker( (Activity)context);
    gpsTracker.getLocation();
    latitude = String.valueOf(gpsTracker.latitude);
    longitude = String.valueOf(gpsTracker.longitude);
    //  AddTrainingPojo.addTrainingPojo.setGpsLoation(latitude + "lat"+"," + longitude+"long");
    AppUtils.getInstance().showLog("location" + latitude + "  " + longitude, SelectShgAdapter.class);

}
    private void saveInPref(int shgRegId, String shgName) {
ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getShgRegistrationId(),shgRegId,context);
ProjectPrefrences.getInstance().saveSharedPrefrecesData(PrefrenceManager.getBundleUsername(),shgName,context);
    }
    void checkingTheRadius() {
        float[] results = new float[1];
        Location.distanceBetween(Double.parseDouble(latitude), Double.parseDouble(longitude), originLat, originLon, results);
        float distanceInMeters = results[0];
        isWithinRange = distanceInMeters < disInMtr;

    }
    void getLocation()
    {
        GPSTracker gpsTracker = new GPSTracker(context);
        if(!NetworkUtils.isInternetOn(context)){
            DialogFactory.getInstance().showAlertDialog(context, R.drawable.ic_launcher_background, "Mela","Gps is not enabled", "Go to seeting", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                }
            }, "", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            },false);
        }else {
            gpsTracker.getLocation();
            latitude = String.valueOf(gpsTracker.latitude);
            longitude = String.valueOf(gpsTracker.longitude);
            Log.d("TAG","Adapter"+latitude+"    "+longitude);

            //  AddTrainingPojo.addTrainingPojo.setGpsLoation(latitude + "lat"+"," + longitude+"long");
            AppUtils.getInstance().showLog("location" + latitude + "  " + longitude, SelectShgAdapter.class);

        }
    }


    private void memberDataRequest() {
        String SHG_DATA_URL = AppConstants.HTTP_TYPE + "://" + AppConstants.HOST+ "/services/mela/data";

        JSONObject shgDataUrlInfo = new JSONObject();
        try {
            shgDataUrlInfo.accumulate("melaId", AppConstants.melaId);
            shgDataUrlInfo.accumulate("shgRegId", "" + shgRegId);
            shgDataUrlInfo.accumulate("mobile", ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyShgMobile(), context));
            shgDataUrlInfo.accumulate("imei_no", ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyImei(), context));
            shgDataUrlInfo.accumulate("device_name", ProjectPrefrences.getInstance().getSharedPrefrencesData(PrefrenceManager.getPrefKeyDeviceInfo(), context));
            shgDataUrlInfo.accumulate("location_coordinate", latitude + "," + longitude);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject encryptedObject = new JSONObject();
        try {
            Cryptography cryptography = new Cryptography();
            String encrypted = cryptography.encrypt(shgDataUrlInfo.toString());
            encryptedObject.accumulate("data", encrypted);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        JsonObjectRequest memberData = new JsonObjectRequest(Request.Method.POST, SHG_DATA_URL, encryptedObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                try {
                    AppUtils.getInstance().showLog("shgdata" + response, SelectShgAdapter.class);
                    Cryptography cryptography = null;
                    String objectResponse = "";
                    if (response.has("data")) {
                        objectResponse = response.getString("data");
                    } else {
                        return;
                    }
                    try {
                        cryptography = new Cryptography();
                        response = new JSONObject(cryptography.decrypt(objectResponse));
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (NoSuchPaddingException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    parseData(response, progressDialog);
                    progressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                AppUtils.getInstance().showLog("error" + error, SelectShgAdapter.class);
                DialogFactory.getInstance().showServerErrorDialog(context, "Server Error!!!", "OK");
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(memberData);
    }

    @Override
    public int getItemCount() {
        return shgData.size();
    }
    private void parseData(JSONObject response,ProgressDialog progressDialog){
        ShgData shgData= ShgData.getInstance();
        ShgData.CrpData crpData=new ShgData.CrpData();
        ShgData.ScData scData=new ShgData.ScData();
        ShgData.MpData mpData=new ShgData.MpData();
        ShgData.HelperData helperData=new ShgData.HelperData();
        if (response.toString().equalsIgnoreCase("{}")){
            progressDialog.dismiss();
            DialogFactory.getInstance().showAlert(context,"No Data Found!!!","OK");
        }else {

            try {

                shgData.setShgRegId(response.getString("shg_reg_id"));
                shgData.setShgCode(response.getString("shg_code"));
                shgData.setShgName(response.getString("shg_name"));

                JSONObject crpObject = response.getJSONObject("CRP_data");
                if (crpObject.has("status")) {
                    crpData.setDataStatus(crpObject.getString("status"));
                } else {
                    crpData.setClosing(crpObject.getInt("closing"));
                    crpData.setOpening(crpObject.getInt("opening"));
                    crpData.setMobile(crpObject.getString("CRP_mobile"));
                    crpData.setUserName(crpObject.getString("user_name"));
                    crpData.setParticipantStatus(crpObject.getString("participant_status"));
                    selectAttedenceTypes.add(crpData);
                }


                JSONObject scObject = response.getJSONObject("sc_data");
                if (scObject.has("status")) {
                    scData.setDataStatus(scObject.getString("status"));
                } else {
                    scData.setClosing(scObject.getInt("closing"));
                    scData.setOpening(scObject.getInt("opening"));
                    scData.setMobile(scObject.getString("CRP_mobile"));
                    scData.setUserName(scObject.getString("user_name"));
                    scData.setParticipantStatus(scObject.getString("participant_status"));
                    selectAttedenceTypes.add(scData);
                }
                JSONObject mainPData = response.getJSONObject("main_participant_data");
                if (mainPData.has("status")) {
                    mpData.setDataStatus(mainPData.getString("status"));
                } else {
                    mpData.setClosing(mainPData.getInt("closing"));
                    mpData.setOpening(mainPData.getInt("opening"));
                    mpData.setMobile(mainPData.getString("CRP_mobile"));
                    mpData.setUserName(mainPData.getString("user_name"));
                    mpData.setParticipantStatus(mainPData.getString("participant_status"));
                    selectAttedenceTypes.add(mpData);
                }


                JSONObject helperObject = response.getJSONObject("helper_data");
                if (helperObject.has("status")) {
                    helperData.setDataStatus(helperObject.getString("status"));
                } else {
                    helperData.setClosing(helperObject.getInt("closing"));
                    helperData.setOpening(helperObject.getInt("opening"));
                    helperData.setMobile(helperObject.getString("CRP_mobile"));
                    helperData.setUserName(helperObject.getString("user_name"));
                    helperData.setParticipantStatus(helperObject.getString("participant_status"));
                    selectAttedenceTypes.add(helperData);
                }
                if(crpObject.has("status") &&scObject.has("status")&& mainPData.has("status")&& helperObject.has("status"))
                {
                    DialogFactory.getInstance().showAlert(context,"We didn't find any data against this SHG.","Ok");
                }else {
                    openDialog(shgData);
                }

            } catch (JSONException je) {
                AppUtils.getInstance().showLog("JSONexception" + je, SelectShgAdapter.class);
            }
        }

    }


    private void openDialog(ShgData shgData) {
       // getAttendanceData();
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.attendance_type_custom);
        selectAttedenceRV = (RecyclerView) dialog.findViewById(R.id.selectAttedenceRV);
        SelectAttendanceTypeAdapter selectAttedenceTypeAdapter = new SelectAttendanceTypeAdapter(selectAttedenceTypes, context, dialog, shgData.getShgRegId());
        selectAttedenceRV.setLayoutManager(new LinearLayoutManager(context));
        selectAttedenceRV.setAdapter(selectAttedenceTypeAdapter);
        selectAttedenceTypeAdapter.notifyDataSetChanged();
        dialog.show();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<SHGEntity> filteredList = new ArrayList<>();
            if (constraint.toString().trim().toLowerCase().isEmpty()) {
                filteredList.addAll(shgDetailForSearch);
            } else {
                for (SHGEntity shg : shgDetailForSearch) {
                    if (shg.shgName.toString().trim().toLowerCase()
                            .contains(constraint.toString().trim().toLowerCase())) {
                        filteredList.add(shg);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            shgData.clear();
            shgData.addAll((Collection<? extends SHGEntity>) results.values);
            notifyDataSetChanged();
        }
    };

    public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView shgName, shgCode, shgMobile, shgRegId, shgStallno;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                shgName = (TextView) itemView.findViewById(R.id.cardViewSelectShgName);
                shgCode = (TextView) itemView.findViewById(R.id.cardViewSelectShgCode);
                shgMobile = (TextView) itemView.findViewById(R.id.cardViewSelectShgMobile);
                shgRegId = (TextView) itemView.findViewById(R.id.cardViewSelectShgRegId);
                shgStallno = (TextView) itemView.findViewById(R.id.cardViewSelectStallNumber);
            }
        }
    }
