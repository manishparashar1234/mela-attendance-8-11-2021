package com.nrlm.melaattendance.ui.home;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.nrlm.melaattendance.repository.AppRepository;
import com.nrlm.melaattendance.repository.BaseRepository;
import com.nrlm.melaattendance.ui.mpin.MpinViewModel;

public class HomeViewModelFactory implements ViewModelProvider.Factory {
    BaseRepository baseRepository;

    public HomeViewModelFactory(BaseRepository baseRepository)
    {
        this.baseRepository=baseRepository;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        T homeViewModel=null;
        if (modelClass.isAssignableFrom(HomeViewModel.class)) {
            homeViewModel = (T) new HomeViewModel((AppRepository) baseRepository);
        }
        return homeViewModel;
    }
}
