package com.nrlm.melaattendance.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.nrlm.melaattendance.BuildConfig;

public class AppDeviceInfoUtils {
    Context context;
    private TelephonyManager telephonyManager;
    AppUtils appUtility;

    public static AppDeviceInfoUtils deviceInfoutils = null;

    public AppDeviceInfoUtils(Context context) {
        this.context = context;
        appUtility =AppUtils.getInstance();
    }

    public static AppDeviceInfoUtils getInstance(Context context) {
        if (deviceInfoutils == null)
            deviceInfoutils = new AppDeviceInfoUtils(context);
        return deviceInfoutils;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public String getIMEINo1() {
        String imeiNo1 = "";
        try {
            if (getSIMSlotCount() > 0) {
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    imeiNo1 = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
                    AppUtils.getInstance().showLog("imeiNo1=" + imeiNo1, AppDeviceInfoUtils.class);
                }else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    imeiNo1 = telephonyManager.getDeviceId(0);
                }

            } else imeiNo1 = telephonyManager.getDeviceId();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        //  return "867130042454246";
        return imeiNo1;
    }


    private int getSIMSlotCount() {
        int getPhoneCount = 0;
        try {
            telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getPhoneCount = telephonyManager.getPhoneCount();
            }
        }catch (Exception e){
            appUtility.showLog("Expection: "+e,AppDeviceInfoUtils.class);
        }
        return getPhoneCount;
    }


    public String getDeviceInfo() {
        String deviceInfo = "";
        try{
            deviceInfo = Build.MANUFACTURER + "-" + Build.DEVICE + "-" + Build.MODEL;
        }catch (Exception e){
            appUtility.showLog("Expection: "+e,AppDeviceInfoUtils.class);

        }

        if (deviceInfo.equalsIgnoreCase("")|| deviceInfo==null)
            return "123-dummy-123";


        return deviceInfo;
    }
    public String getAppVersion() {
        String appVersion = "";
        try {
            appVersion =  BuildConfig.VERSION_NAME;;
        }catch (Exception e){
            appUtility.showLog("Expection: "+e,AppDeviceInfoUtils.class);
        }
        return appVersion;
    }

    public String getApiVersion(){
        int version = 0;
        try {
            version =  Build.VERSION.SDK_INT;
        }catch (Exception e){
        }
        return String.valueOf(version);
    }


}
